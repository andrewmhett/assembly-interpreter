# quasi-asm

QuasiASM is a quasi-assembly, interpreted language. This is the repository for the interpreter for QuasiASM, written in C. QuasiASM makes use of 26 named registers (a-z) as well as two special registers (pc and ra). This interpreter also has the ability to print messages to stdout with the msg instruction and read stdin input with the uin instruction.

## Registers
```
Register  Description
[a-z]     Named alphabetical registers. Signed 64-bit integers.
pc        Program counter. Read-only.
ra        Return address. Contains the pc value (+1) of the last jump.
```


## Instruction Set
```
Instruction	Operands							Description
MOV		[(<int> or <register>) <register>]				Moves a value to a register.
ADD		[(<int> or <register>) <register>]				Adds a value to a register.
SUB		[(<int> or <register>) <register>]				Subtracts a value from a register.
MUL		[(<int> or <register>) <register>]				Multiplies the value in a register by another value.
DIV		[(<int> or <register>) <register>]				Divides the value in a register by another value.
CMP		[(<int> or <register>) (<int or <register>)]			Saves the difference between two operands for later use by conditional jumps.
JMP		[<label>]							Jumps the interpreter to a specific label.
JLT		[<label>]                                                       Jumps the interpreter to a specific label if the difference stored by CMP is less than zero.
JGT		[<label>]                                                       Jumps the interpreter to a specific label if the difference stored by CMP is greater than zero.
JET		[<label>]                                                       Jumps the interpreter to a specific label if the difference stored by CMP is equal to zero.
JRO		[(<int> or <register>)]                                         Jumps the interpreter with a specified relative offest.
MSG		[<any> ...]							Formats and prints a message to stdout with any number of operands.
UIN		[<any> ... <format string> <register>]				Formats and prints a prompt to stdout and reads stdin into the specified register.
SPU		[<int> or <register>]						Pushes the supplied value to the stack.
SPO		[<register>]							Pops the supplied value from the stack and moves its value to the specified register.
```
