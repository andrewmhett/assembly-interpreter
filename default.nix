with import <nixpkgs> {};

stdenv.mkDerivation {
	pname = "quasi-asm";
	version = "1.0.0";
	buildInputs = [
          gcc
          check
          subunit
	];
	src = ./.;
}
