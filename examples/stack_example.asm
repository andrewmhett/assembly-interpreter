; This example pushes five values [1-5]
; to the stack and then pops those values
; from the stack

mov 0 b
push_loop:
add 1 b
spu b
msg "Pushing top of stack: " b "\n"
cmp b 5
jlt push_loop
mov 0 b
pop_loop:
add 1 b
spo a
msg "Popping top of stack: " a "\n"
cmp b 5
jlt pop_loop