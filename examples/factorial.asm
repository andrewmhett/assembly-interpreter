; This is an example of a simple recursive procedure. The uin
; instruction is used to read user input into the a register.
; The 'fact' procedure takes an input from the a register and
; sets the a register to a! recursively.

receive_input:
uin "Enter a non-negative integer to compute the factorial of: " %d a ; set the a register to the input value
mov a c
cmp a 0
jlt invalid_input
jet input_is_zero
jmp fact ; call 'fact' with the input value

msg c "! = " a "\n" ; print the final result of 'fact'
jmp end ; jump to the end

; this procedure takes a value from the a register and returns
; its result through the a register as well
fact:
spu a ; first, push a
spu ra ; next, push ra
cmp a 1
jet epilogue ; we've reached the base case; jump to the epilogue
sub 1 a ; decrement a by 1
jgt fact ; if we haven't reached the base case, call 'fact' again
epilogue:
spo ra ; pop the ra value
spo b ; pop the a value
mul b a ; multiply a by the popped a value
jmp ra ; jump to the ra address popped from the stack

input_is_zero:
mov 1 a
jmp ra

invalid_input:
msg "Invalid input.\n"
jmp receive_input

end: