; This is an example of a conditional
; loop which computes N number of elements
; in the fibonacci sequence

; prompt the user for the number of iterations
uin "Enter the number of iterations to compute: " %lld e 

mov 1 b
fib:
msg "F" d ": " c "\n"
add 1 d
mov b a
mov c b
mov 0 c
add a c
add b c
cmp d e
jlt fib