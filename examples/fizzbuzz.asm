; This is a more complicated example using a couple of
; procedures. The fizz and buzz procedures
; utilize the ra register to jump back to the caller.

uin "Enter the number of iterations to run: " %lld d

loop:
mov 0 c ; reset the output flag
add 1 a ; increment the counter

; test fizz case next
mov a b
div 3 b
mul 3 b
cmp a b
jet fizz

; finally, test buzz case
mov a b
div 5 b
mul 5 b
cmp a b
jet buzz

cmp c 1 ; if either 'fizz' or 'buzz' printed, skip over the counter print
jet loop_end

; if none of the above cases were reached, print the number
msg a

loop_end:
msg "\n" ; print a newline before the next iteration
cmp a d
jlt loop

jmp end

fizz:
msg "fizz"
mov 1 c ; set the output flag to 1
jmp ra ; jump back to the caller

buzz:
msg "buzz"
mov 1 c ; set the output flag to 1
jmp ra ; jump back to the caller

end: ; label that we jump to once we reach the final value in the loop