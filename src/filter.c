#include <string.h>
#include <stdint.h>

#include "definitions.h"
#include "filter.h"

program_counter_t filter_program(const char* unfiltered_assembly_program, char* assembly_program) {
	uint32_t char_position = 0;
	uint8_t newline = 0;
	uint8_t comment = 0;
	uint8_t in_string = 0;
	uint8_t newline_last_written = 1;
	//Loop through all of the characters in the program
	//and remove empty lines and comments
	for (int i = 0; i < strlen(unfiltered_assembly_program)+1; i++) {
		if (unfiltered_assembly_program[i] == '\r') continue;
		if (unfiltered_assembly_program[i] == COMMENT_CHARACTER) {
			comment = 1;
		}
		newline = 0;
		while (unfiltered_assembly_program[i] == '\n' && unfiltered_assembly_program[i+1] != COMMENT_CHARACTER) {
			i++;
			newline = 1;
		}
		if (!comment && unfiltered_assembly_program[i] == STRING_CHARACTER) {
			if (i > 0 && unfiltered_assembly_program[i-1] == '\\') {
				in_string = !in_string;
			}
			in_string = !in_string;
		}
		if (newline) {
			comment = 0;
			in_string = 0;
			i--;
		}
		if (!comment) {
			uint8_t skip_newline = 0;
			if (unfiltered_assembly_program[i] == '\n' && newline_last_written) {
				skip_newline = 1;
			}
			if (unfiltered_assembly_program[i] == LABEL_TERMINATOR && !in_string) {
				assembly_program[char_position] = LABEL_TERMINATOR;
				char_position++;
				while (unfiltered_assembly_program[i] != '\n' && unfiltered_assembly_program[i] != 0) {
					i++;
				}
			}
			if (unfiltered_assembly_program[i] == 0 && !newline_last_written) {
				assembly_program[char_position] = '\n';
				break;
			}
			if (!skip_newline) {
				if (unfiltered_assembly_program[i] == '\n') {
					newline_last_written = 1;
				} else {
					newline_last_written = 0;
				}
				assembly_program[char_position]=unfiltered_assembly_program[i];
				char_position++;
			}
		}
	}
	if (comment) {
		assembly_program[char_position] = 0;
	}
	assembly_program[char_position + 1] = 0;
	//Count the number of \n characters to find
	//the program's length
	uint32_t program_length = 0;
	for (int i = 0; i < strlen(assembly_program); i++) {
		if (assembly_program[i]=='\n') {
			program_length++;
		}
	}
	if (program_length == 0 && strlen(assembly_program) > 0) {
		program_length = 1;
	}
    return program_length;
}