#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "interpret.h"

/*
 * EXIT CODES
 *   0:	SUCCESS
 * - 1: INVALID INPUT FILE
 * - 2: INVALID # CLI ARGUMENTS
 * -10:	INVALID INSTRUCTION
 * -20:	INVALID NUMBER OF OPERANDS
 * -30: INVALID OPERAND
 * -40: INAVLID I/O ARGUMENT
 * -50: EMPTY STACK, ATTEMPTED POP
 */

int main(int c, const char* argv[]) {
	int exit_code = 0;
	long length;
	if (c == 2) {
		FILE * f = fopen(argv[1], "rb");
		if (f) {
			fseek(f, 0, SEEK_END);
			length = ftell(f);
			fseek(f, 0, SEEK_SET);
			char assembly_input[length + 1];
			memset(assembly_input, 0, length + 1);
			fread(&assembly_input, 1, length, f);
			fclose(f);
			exit_code = interpret(&assembly_input[0]);
		} else {
			exit_code = -1;
		}
	} else {
		exit_code = -2;
	}
	return exit_code;
}
