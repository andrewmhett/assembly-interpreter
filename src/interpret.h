#ifndef INTERPRET_H
#define INTERPRET_H

#include <stdlib.h>

#include "definitions.h"
#include "pair.h"
#include "label.h"
#include "registers.h"
#include "list.h"

void memoize_segments(char*[], ListNode*[], size_t);
void memoize_instructions(ListNode*[], int*, size_t);
void memoize_values(ListNode*[], pair*, pair*, ListNode*, Registers*, size_t);
void cleanup(ListNode*, ListNode*, ListNode**, size_t);
int check_for_errors(ListNode*[], size_t, Registers*);
int interpret(const char*);

#endif