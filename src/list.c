#include <stdlib.h>

#include "list.h"

void list_append(ListNode** node, void* data) {
    if (*node == NULL) {
        *node = (ListNode*)malloc(sizeof(ListNode));
        (*node)->next = NULL;
        (*node)->data = data;
        return;
    }
    ListNode* current_node = *node;
    if (current_node->next == NULL) {
        current_node->next = (ListNode*)malloc(sizeof(ListNode));
        current_node->next->next = NULL;
        current_node->next->data = data;
        return;
    }
    list_append(&((*node)->next), data);
}

ListNode* list_peek_tail(ListNode* node) {
    if (node == NULL) return NULL;
    if (node->next == NULL) return node;
    return list_peek_tail(node->next);
}

ListNode* list_peek_n(ListNode* node, int n) {
    if (node == NULL) return NULL;
    if (n == 0) return node;
    return list_peek_n(node->next, n - 1);
}

void* list_pop_tail(ListNode** node) {
    if (*node == NULL) return NULL;
    if ((*node)->next == NULL) {
        void* data = (*node)->data;
        free(*node);
        *node = NULL;
        return data;
    }
    return list_pop_tail(&(*node)->next);
}

size_t list_length(ListNode* node, size_t length) {
    if (node == NULL) return length;
    return list_length(node->next, length + 1);
}

void list_free(ListNode* node) {
    if (node == NULL) return;
    ListNode* next = node->next;
    free(node->data);
    free(node);
    list_free(next);
}