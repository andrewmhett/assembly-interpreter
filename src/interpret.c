#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include "definitions.h"
#include "interpret.h"
#include "label.h"
#include "list.h"
#include "instructions.h"
#include "registers.h"
#include "filter.h"
#include "pair.h"

void memoize_segments(char* lines[], ListNode* segments[], size_t program_length) {
	for (int i = 0; i < program_length; i++) {
		char* current_line = lines[i];
		for (int pass = 0; pass < 2; pass++) {
			int within_quotes = 0;
			int char_position = 0;
			ListNode** this_segment = &segments[i];
			uint8_t skip = 0;
			uint8_t end_of_segment = 0;
			uint8_t end_of_line = 0;
			for (int o = 0; o < strlen(current_line) + 1; o++) {
				if (strlen(current_line) == 0) break;
				skip = 0;
				end_of_line = 0;
				if ((current_line[o] == '	' || current_line[o] == ' ') && !within_quotes) {
					skip=1;
					if (pass == 1) {
						((char*)((*this_segment)->data))[char_position] = '\0';
					}
					end_of_segment=1;
					end_of_line = 1;
					for (int j = o; j < strlen(current_line); j++) {
						if (current_line[j] != '	' && current_line[j] != ' ') {
							end_of_line = 0;
							break;
						}
					}
				}
				if (end_of_line) {
					break;
				}
				if (current_line[o] == STRING_CHARACTER) {
					skip=1;
					within_quotes = !within_quotes;
				}
				if (!skip) {
					if (end_of_segment) {
						if (pass == 0) {
							list_append(&segments[i], (void*)malloc(char_position + 1));
						} 
						this_segment = &(*this_segment)->next;
						char_position=0;
						end_of_segment=0;
					}
					if (pass == 1) {
						((char*)((*this_segment)->data))[char_position] = current_line[o];
					}
					char_position++;
				}
			}
			if (pass == 0) {
				if (char_position != 0) list_append(&segments[i], (void*)malloc(char_position + 1));
			}
		}
	}
}

void memoize_instructions(ListNode* segments[], int* instructions, size_t program_length) {
	for (int i = 0; i < program_length; i++) {
		if (segments[i] == NULL) continue;
		instructions[i] = lookup_instruction((char*)((segments[i])->data));
	}
}

void memoize_values(ListNode* segments[], pair* lvalues, pair* second_cmp_values, ListNode* label_list_head, Registers* reg_struct, size_t program_length) {
	for (int i = 0; i < program_length; i++) {
		ListNode* segment_list_head = segments[i];	
		if (segment_list_head == NULL) continue;
		switch (lookup_instruction((char*)(segment_list_head->data))) {
			case MOV:
			case ADD:
			case SUB:
			case MUL:
			case DIV:
			case SPU:
			case JRO:
			case CMP:
			{
				ListNode* second_node = segments[i]->next;
				if (second_node == NULL) break;
				char* second_segment = (char*)(second_node->data);
				if (lookup_reg(second_segment, reg_struct) != NULL) {
					lvalues[i].first = 1;
					lvalues[i].second = (register_t)lookup_reg(second_segment, reg_struct);
				} else {
					lvalues[i].first = 0;
					lvalues[i].second = atoi(second_segment);
				}
				ListNode* third_node = second_node->next;
				if (third_node == NULL) break;
				char* third_segment = (char*)(third_node->data);
				if (lookup_reg(third_segment, reg_struct) != NULL) {
					second_cmp_values[i].first = 1;
					second_cmp_values[i].second = (register_t)lookup_reg(third_segment, reg_struct);
				} else {
					second_cmp_values[i].first = 0;
					second_cmp_values[i].second = atoi(third_segment);
				}
				break;
			}
			case JMP:
			case JET:
			case JGT:
			case JLT:
			{
				ListNode* second_node = segments[i]->next;
				if (second_node == NULL) break;
				char* second_segment = (char*)(second_node->data);
				if (lookup_reg(second_segment, reg_struct) != NULL) {
					lvalues[i].first = 1;
					lvalues[i].second = (register_t)lookup_reg(second_segment, reg_struct);
				} else {
					lvalues[i].first = 0;
					if (lookup_label_position(second_segment, label_list_head) == -1) {
						lvalues[i].second = atoi(second_segment);
					} else {
						lvalues[i].second = lookup_label_position(second_segment, label_list_head);
					}
				}
				break;
			}
		}
	}
}

int check_for_errors(ListNode* segments[], size_t program_length, Registers* reg_struct) {
	int exit_code = 0;
	//Make sure all the instructions are valid and in the right format
	for (int i = 0; i < program_length; i++) {
		ListNode* segment_list_head = segments[i];
		if (segment_list_head == NULL) continue;
		int current_instruction = lookup_instruction((char*)(segment_list_head->data));
		//Make sure the current instruction is valid
		if (current_instruction == INVALID) {
			exit_code = -10;
			break;
		}
		//If the instruction is fixed-width, check that it has the correct number of operands
		if (lookup_num_operands(current_instruction) != -1) {
			if (lookup_num_operands(current_instruction) != list_length(segment_list_head, 0)-1) {
				exit_code = -20;
				break;
			}
		}
		//If an operand is of an invalid format, set the exit code to -30
		switch (current_instruction) {
		case MOV:
		case ADD:
		case SUB:
		case MUL:
		case DIV:
		case SPO:
			if (lookup_reg((char*)(list_peek_tail(segment_list_head)->data), reg_struct) == NULL) {
				exit_code = -30;
			} else if (lookup_reg((char*)(list_peek_tail(segment_list_head)->data), reg_struct) == reg_struct->program_counter) {
				exit_code = -30;
			}
			break;
		case JMP:
		case JLT:
		case JET:
		case JGT:
			break;
		}
		//If the instruction has a register/integer value as an operand, make sure it's valid
		switch (current_instruction) {
		case MOV:
		case ADD:
		case SUB:
		case MUL:
		case DIV:
		case JRO:
		case SPU:
		{
			char* operand_1 = (char*)(list_peek_n(segment_list_head, 1)->data);
			if (lookup_reg(operand_1, reg_struct) == NULL && atoi(operand_1) == 0 && strcmp(operand_1,"0") != 0) {
				exit_code = -30;
			}
			break;
		}
		case UIN: //special case; the last operand must be a writable register
			if (lookup_reg((char*)(list_peek_tail(segment_list_head)->data), reg_struct) == NULL
			        || lookup_reg((char*)(list_peek_tail(segment_list_head)->data), reg_struct) == reg_struct->program_counter) {
				exit_code = -30;
			}
		}
	}
	return exit_code;
}

void cleanup(ListNode* stack_head, ListNode* label_list_head, ListNode* segments[], size_t program_length) {
	// free the memory allocated to the stack
	list_free(stack_head);

	// free the memory allocated to the label list
	ListNode* label_node = label_list_head;
	while (label_node != NULL) {
		free(((Label*)(label_node->data))->name);
		label_node = label_node->next;
	}
	list_free(label_list_head);

	// free the memory allocated to the line segments
	for (int i = 0; i < program_length; i++) {
		list_free(segments[i]);
	}
}

int interpret(const char* unfiltered_assembly_program) {
	int exit_code = 0;
	Registers reg_struct;
	register_t program_counter;
	register_t return_address;
	register_t alpha_registers[26];
	reg_struct.alpha_registers = alpha_registers;
	reg_struct.program_counter = &program_counter;
	reg_struct.return_address = &return_address;
	ListNode* label_list_head = NULL;
	ListNode* stack_head = NULL;
	initialize_registers(&reg_struct);
	char assembly_program[strlen(unfiltered_assembly_program) + 2];
	memset(assembly_program, 0, sizeof(assembly_program));
	program_counter_t program_length = filter_program(unfiltered_assembly_program, assembly_program);
	char* lines[program_length];
	memset(&lines[0], 0, sizeof(lines));
	size_t size = sizeof(assembly_program);
	FILE* program_stream = fmemopen(assembly_program, size, "r");
	for (size_t line = 0; line < program_length; line++) {
		char* linebuf = NULL;
		size_t size = 0;
		if (getline(&linebuf, &size, program_stream) == -1) {
			free(linebuf);
		} else {
			lines[line] = linebuf;
			lines[line][strlen(lines[line]) - 1] = '\0';
		}
	}
	size_t byte_offset = 0;
	for (size_t line = 0; line < program_length; line++) {
		if (lines[line] == NULL) continue;
		int label = 0;
		int i=strlen(lines[line]) - 1;
		if (i == -1) continue;
		while (i >=0 && (lines[line][i] == '	' || lines[line][i] == ' ')) i--;
		if (lines[line][i]==LABEL_TERMINATOR) {
			label = 1;
		}
		size_t line_length = strlen(lines[line]);
		if (label) {
			char* name_buf = NULL;
			size_t size = 0;
            fseek(program_stream, byte_offset, SEEK_SET);
			if (getdelim(&name_buf, &size, LABEL_TERMINATOR, program_stream) == -1) {
				free(name_buf);
			} else {
				Label* new_label = malloc(sizeof(Label));
				new_label->name = name_buf;
				new_label->name[strlen(name_buf) - 1] = '\0';
				new_label->position = line;
				list_append(&label_list_head, (void*)new_label);
			}
			lines[line][0] = '\0';
		}
		byte_offset += line_length + 1;
	}
	fclose(program_stream);
	ListNode* segments[program_length];
	memset(&segments[0], 0, sizeof(segments));
	int instructions[program_length];
	memset(instructions, 0, sizeof(instructions));
	pair lvalues[program_length];
	memset(&lvalues[0], 0, sizeof(lvalues));
	pair second_cmp_values[program_length];
	memoize_segments(lines, segments, program_length);
	exit_code = check_for_errors(segments, program_length, &reg_struct);
	if (exit_code != 0) {
		cleanup(stack_head, label_list_head, segments, program_length);
		return exit_code;
	}
	// Now we know that the program is valid
	// We don't need lines[]* anymore, so we can deallocate them
	for (size_t line = 0; line < program_length; line++) {
		free(lines[line]);
	}
	memoize_instructions(segments, instructions, program_length);
	memoize_values(segments, lvalues, second_cmp_values, label_list_head, &reg_struct, program_length);
	int comparison_diff = 0;
	register_t lvalue = 0;
	for (program_counter = 0; program_counter < program_length; program_counter++) {
		if (exit_code != 0) break;
		//If the instruction has a register/integer value as an lvalue, recall its value from memory
		switch (lvalues[program_counter].first) {
		case 1:
			lvalue = *((register_t*)lvalues[program_counter].second);
			break;
		case 0:
			lvalue = lvalues[program_counter].second;
			break;
		}
		switch (instructions[program_counter]) {
		case NOP:
			break;
		case MOV:
			*lookup_reg((char*)(list_peek_tail(segments[program_counter])->data), &reg_struct) = lvalue;
			break;
		case ADD:
			*lookup_reg((char*)(list_peek_tail(segments[program_counter])->data), &reg_struct) += lvalue;
			break;
		case SUB:
			*lookup_reg((char*)(list_peek_tail(segments[program_counter])->data), &reg_struct) -= lvalue;
			break;
		case MUL:
			*lookup_reg((char*)(list_peek_tail(segments[program_counter])->data), &reg_struct) *= lvalue;
			break;
		case DIV:
			*lookup_reg((char*)(list_peek_tail(segments[program_counter])->data), &reg_struct) /= lvalue;
			break;
		case JMP:
			return_address = program_counter+1;
			program_counter = lvalue;
			program_counter--;
			break;
		case CMP:
			comparison_diff = lvalue;
			switch (second_cmp_values[program_counter].first) {
				case 1:
					comparison_diff -= *((register_t*)second_cmp_values[program_counter].second);
					break;
				case 0:
					comparison_diff -= second_cmp_values[program_counter].second;
					break;
			}
			break;
		case JLT:
			if (comparison_diff<0) {
				return_address = program_counter+1;
				program_counter = lvalue;
				program_counter--;
			}
			break;
		case JGT:
			if (comparison_diff>0) {
				return_address = program_counter+1;
				program_counter = lvalue;
				program_counter--;
			}
			break;
		case JET:
			if (comparison_diff==0) {
				return_address = program_counter+1;
				program_counter = lvalue;
				program_counter--;
			}
			break;
		case JRO:
			return_address = program_counter+1;
			program_counter--;
			program_counter+=lvalue;
			break;
		case MSG:
		{
			//construct the output string
			ListNode* segment_node = segments[program_counter];
			if (segment_node != NULL) segment_node = segment_node->next;
			while (segment_node != NULL) {
				if (lookup_reg((char*)(segment_node->data), &reg_struct) != NULL) {
					printf("%" PRId64, *lookup_reg((char*)(segment_node->data), &reg_struct));
				} else {
					char* current_string = (char*)(segment_node->data);
					for (int j = 0; j < strlen((char*)segment_node->data); j++) {
						if (current_string[j] == '\\') {
							current_string[j] = ((char*)(segment_node->data))[j + 1] - 100;
							j++;
							for (int k = j; k < strlen((char*)segment_node->data); k++) {
								((char*)(segment_node->data))[k] = ((char*)(segment_node->data))[k + 1];
							}
						}
					}
					printf("%s", (char*)(segment_node->data));
				}
				segment_node = segment_node->next;
			}
			break;
		}
		case UIN:
		{
			ListNode* segment_node = segments[program_counter];
			size_t segment_length = list_length(segments[program_counter], 0);
			if (segment_node != NULL) segment_node = segment_node->next;
			for (int i = 0; i < segment_length - 3 && segment_node != NULL; i++) {
				if (lookup_reg((char*)(segment_node->data), &reg_struct) != NULL) {
					printf((char*)(list_peek_n(segments[program_counter], i-1)->data), *lookup_reg((char*)(segment_node->data), &reg_struct));
				} else {
					for (int j = 0; j < sizeof(segment_node->data); j++) {
						if (((char*)(segment_node->data))[j] == '\\') {
							((char*)(segment_node->data))[j] = ((char*)(segment_node->data))[j + 1] - 100;
							j++;
							for (int k = j; k < sizeof(segment_node); k++) {
								((char*)(segment_node->data))[k] = ((char*)(segment_node->data))[k + 1];
							}
						}
					}
					printf("%s",(char*)(segment_node->data));
				}
				segment_node = segment_node->next;
			}
			char* format_string = (char*)(segment_node->data);
			segment_node = segment_node->next;
			register_t* destination_reg = lookup_reg((char*)(segment_node->data), &reg_struct);
			scanf(format_string, destination_reg);
			break;
		}
		case SPU:
		{
			register_t* stack_value = malloc(sizeof(register_t));
			*stack_value = lvalue;
			list_append(&stack_head, stack_value);
			break;
		}
		case SPO:
		{
			register_t* head_value = list_pop_tail(&stack_head);
			if (head_value == NULL) {
				exit_code = -50;
				break;
			}
			*lookup_reg((char*)(segments[program_counter]->next->data), &reg_struct) = *head_value;
			free(head_value);
			break;
		}
		}
	}

	cleanup(stack_head, label_list_head, segments, program_length);

	return exit_code;
}