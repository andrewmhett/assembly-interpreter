#ifndef PAIR_H
#define PAIR_H

#include "definitions.h"

typedef struct pair {
    register_t first;
    register_t second;
} pair;

#endif