#include <string.h>

#include "instructions.h"

int lookup_instruction(char* instruction_name){
	int instruction = INVALID;
	if (strlen(instruction_name) == 0) return NOP;
	if (strlen(instruction_name) != 3) return INVALID;
	switch (instruction_name[0]){
		case 'm':
			switch (instruction_name[1]) {
				case 'o':
					if (instruction_name[2] == 'v') instruction = MOV;
					break;
				case 'u':
					if (instruction_name[2] == 'l') instruction = MUL;
					break;
				case 's':
					if (instruction_name[2] == 'g') instruction = MSG;
					break;
			}
			break;
		case 'a':
			switch (instruction_name[1]) {
				case 'd':
					if (instruction_name[2] == 'd') instruction = ADD;
					break;
			}
			break;
		case 's':
			switch (instruction_name[1]) {
				case 'u':
					if (instruction_name[2] == 'b') instruction = SUB;
					break;
				case 'p':
					if (instruction_name[2] == 'u') instruction = SPU;
					if (instruction_name[2] == 'o') instruction = SPO;
					break;
			}
			break;
		case 'd':
			switch (instruction_name[1]) {
				case 'i':
					if (instruction_name[2] == 'v') instruction = DIV;
					break;
			}
			break;
		case 'j':
			switch (instruction_name[1]) {
				case 'm':
					if (instruction_name[2] == 'p') instruction = JMP;
					break;
				case 'l':
					if (instruction_name[2] == 't') instruction = JLT;
					break;
				case 'g':
					if (instruction_name[2] == 't') instruction = JGT;
					break;
				case 'e':
					if (instruction_name[2] == 't') instruction = JET;
					break;
				case 'r':
					if (instruction_name[2] == 'o') instruction = JRO;
					break;
			}
			break;
		case 'c':
			switch (instruction_name[1]) {
				case 'm':
					if (instruction_name[2] == 'p') instruction = CMP;
					break;
			}
			break;
		case 'u':
			switch (instruction_name[1]) {
				case 'i':
					if (instruction_name[2] == 'n') instruction = UIN;
					break;
			}
			break;
		case 'n':
			instruction = NOP;
			break;
	}
	return instruction;
}

int lookup_num_operands(int instruction) {
	int num_operands = 0;
	switch (instruction) {
		case MOV:
		case ADD:
		case SUB:
		case MUL:
		case DIV:
		case CMP:
			num_operands = 2;
			break;
		case JMP:
		case JLT:
		case JGT:
		case JET:
		case JRO:
		case SPU:
		case SPO:
			num_operands = 1;
			break;
		case MSG:
		case UIN:
			num_operands = -1;
			break;
		case NOP:
			num_operands = 0;
			break;
	}
	return num_operands;
}