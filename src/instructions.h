#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H

enum Instruction {INVALID,MOV,ADD,SUB,MUL,DIV,JMP,CMP,JLT,JGT,JET,JRO,MSG,UIN,SPU,SPO,NOP};

int lookup_instruction(char*);
int lookup_num_operands(int);

#endif