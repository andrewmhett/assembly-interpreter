#ifndef REGISTERS_H
#define REGISTERS_H

#include "definitions.h"

typedef struct Registers {
    register_t* alpha_registers;
    register_t* program_counter;
    register_t* return_address;
} Registers;

void initialize_registers(Registers*);
register_t* lookup_reg(char*, Registers*);

#endif