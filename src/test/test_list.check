#include <stdlib.h>

#include "../src/list.h"

#suite List Tests

#test test_list_append
    ListNode* list_head = NULL;
    int* data_1 = malloc(sizeof(int));
    int* data_2 = malloc(sizeof(int));
    int* data_3 = malloc(sizeof(int));
    list_append(&list_head, data_1);
    list_append(&list_head, data_2);
    list_append(&list_head, data_3);
    fail_unless(list_head != NULL);
    fail_unless((int*)(list_head->data) == data_1);
    fail_unless(list_head->next != NULL);
    fail_unless((int*)(list_head->next->data) == data_2);
    fail_unless(list_head->next->next != NULL);
    fail_unless((int*)(list_head->next->next->data) == data_3);
    list_free(list_head);

#test test_list_peek_tail
    ListNode* list_head = NULL;
    int* data_1 = malloc(sizeof(int));
    int* data_2 = malloc(sizeof(int));
    int* data_3 = malloc(sizeof(int));
    list_append(&list_head, data_1);
    fail_unless((int*)(list_peek_tail(list_head)->data) == data_1);
    list_append(&list_head, data_2);
    fail_unless((int*)(list_peek_tail(list_head)->data) == data_2);
    list_append(&list_head, data_3);
    fail_unless((int*)(list_peek_tail(list_head)->data) == data_3);
    list_free(list_head);
    fail_unless(list_peek_tail(NULL) == NULL);

#test test_list_peek_n
    ListNode* list_head = NULL;
    int* data_1 = malloc(sizeof(int));
    int* data_2 = malloc(sizeof(int));
    int* data_3 = malloc(sizeof(int));
    list_append(&list_head, data_1);
    list_append(&list_head, data_2);
    list_append(&list_head, data_3);
    fail_unless(list_peek_n(list_head, 0) != NULL);
    fail_unless((int*)(list_peek_n(list_head, 0)->data) == data_1);
    fail_unless(list_peek_n(list_head, 1) != NULL);
    fail_unless((int*)(list_peek_n(list_head, 1)->data) == data_2);
    fail_unless(list_peek_n(list_head, 2) != NULL);
    fail_unless((int*)(list_peek_n(list_head, 2)->data) == data_3);
    fail_unless(list_peek_n(list_head, -1) == NULL);
    fail_unless(list_peek_n(list_head, 3) == NULL);
    list_free(list_head);

#test test_list_pop_tail
    ListNode* list_head = NULL;
    int* data_1 = malloc(sizeof(int));
    int* data_2 = malloc(sizeof(int));
    int* data_3 = malloc(sizeof(int));
    list_append(&list_head, data_1);
    list_append(&list_head, data_2);
    list_append(&list_head, data_3);
    fail_unless((int*)list_pop_tail(&list_head) == data_3);
    fail_unless((int*)list_pop_tail(&list_head) == data_2);
    fail_unless((int*)list_pop_tail(&list_head) == data_1);
    fail_unless(list_head == NULL);
    fail_unless((int*)list_pop_tail(&list_head) == NULL);
    free(data_1);
    free(data_2);
    free(data_3);

#test test_list_length
    ListNode* list_head = NULL;
    int* data_1 = malloc(sizeof(int));
    int* data_2 = malloc(sizeof(int));
    int* data_3 = malloc(sizeof(int));
    fail_unless(list_length(list_head, 0) == 0);
    list_append(&list_head, data_1);
    fail_unless(list_length(list_head, 0) == 1);
    list_append(&list_head, data_2);
    fail_unless(list_length(list_head, 0) == 2);
    list_append(&list_head, data_3);
    fail_unless(list_length(list_head, 0) == 3);
    list_free(list_head);