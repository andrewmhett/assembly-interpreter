#include <string.h>

#include "definitions.h"
#include "label.h"
#include "list.h"

int lookup_label_position(char* name, ListNode* node) {
	if (node == NULL) return -1;
	Label* current_label = (Label*)node->data;
	if (strcmp(name, current_label->name) == 0) return current_label->position;
	return lookup_label_position(name, node->next);
}