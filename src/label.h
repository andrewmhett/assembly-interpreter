#ifndef LABEL_H
#define LABEL_H

#include "list.h"

typedef struct Label {
	char* name;
	int position;
} Label;

void initialize_labels(Label*);
int lookup_label_position(char*, ListNode*);

#endif