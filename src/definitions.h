#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <stdint.h>

#define program_counter_t       size_t
#define register_t              int64_t

#define COMMENT_CHARACTER       ';'
#define STRING_CHARACTER        '"'
#define LABEL_TERMINATOR        ':'

#endif