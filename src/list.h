#ifndef LIST_H
#define LIST_H

typedef struct ListNode {
    void* data;
    struct ListNode* next;
} ListNode;

void list_append(ListNode**, void*);
ListNode* list_peek_tail(ListNode*);
ListNode* list_peek_n(ListNode*, int);
void* list_pop_tail(ListNode**);
size_t list_length(ListNode*, size_t);
void list_free(ListNode*);

#endif