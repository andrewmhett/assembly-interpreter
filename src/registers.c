#include <stddef.h>
#include <string.h>

#include "definitions.h"
#include "registers.h"

void initialize_registers(Registers* reg_struct) {
	*reg_struct->program_counter = 0;
	*reg_struct->return_address = 0;
	for (int i = 0; i < 26; i++) {
		reg_struct->alpha_registers[i] = 0;
	}
}

register_t* lookup_reg(char* reg_name, Registers* reg_struct) {
	register_t* ret_reg = NULL;
	switch (strlen(reg_name)) {
		case 1:
			if (reg_name[0]>='a' && reg_name[0]<='z') {
				ret_reg = &(reg_struct->alpha_registers[reg_name[0]-'a']);
			}
			break;
		case 2:
			if (strcmp(reg_name, "pc") == 0) {
				ret_reg = reg_struct->program_counter;
			}
			if (strcmp(reg_name, "ra") == 0) {
				ret_reg = reg_struct->return_address;
			}
			break;
		}
	return ret_reg;
}