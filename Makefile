CC		= gcc
CFLAGS		= -Wall -Wstrict-prototypes -Wundef -Wmissing-declarations -Wmissing-prototypes -std=gnu17 -O3
T_LDFLAGS	= -lcheck -lsubunit -lm -pthread
SRCDIR		= src
TESTDIR		= $(SRCDIR)/test
CHECKDIR	= .check
BUILDDIR	= build
T_BUILDDIR	= tests
PROGRAM		= $(BUILDDIR)/interpreter
INSTALL_DIR	= ~/.local

$(PROGRAM): $(SRCDIR) $(BUILDDIR) $(SRCDIR)/*
	$(CC) $(CFLAGS) $(SRCDIR)/*.c -o $@

.PHONY: test install clean

test: $(T_BUILDDIR)
	# Executing tests...
	ret_code=0; \
	for test_src in `ls $(CHECKDIR)` ; do \
		echo "" ; \
		$(T_BUILDDIR)/`echo $$test_src | sed "s/.c$$//"` || ret_code=1 ; \
	done ; \
	exit $${ret_code}

install: $(PROGRAM)
	@if [ -z "${out}" ]; then\
		mkdir -p $(INSTALL_DIR)/bin;\
		install $(PROGRAM) $(INSTALL_DIR)/bin/quasi-asm;\
	else\
		mkdir -p ${out}/bin;\
		install $(PROGRAM) ${out}/bin/quasi-asm;\
	fi

$(T_BUILDDIR)/%: $(CHECKDIR)/%.c $(T_BUILDDIR)
	$(CC) $(CFLAGS) `ls $(SRCDIR)/*\.c | tr "\n" " " | sed "s/$(SRCDIR)\/main\.c //"` $< -o $@ $(T_LDFLAGS)

$(CHECKDIR)/%.c: $(TESTDIR)/%.check $(CHECKDIR)
	checkmk $< > $@

$(CHECKDIR):
	mkdir -p $(CHECKDIR)
	for test_src in `ls $(TESTDIR)` ; do \
		make $(CHECKDIR)/`echo $$test_src | sed "s/.check$$/.c/"` ; \
	done

$(T_BUILDDIR): $(CHECKDIR)
	mkdir -p $(T_BUILDDIR)
	for test_src in `ls $(CHECKDIR)` ; do \
		make $(T_BUILDDIR)/`echo $$test_src | sed "s/.c$$//"` ; \
	done

$(BUILDDIR):
	mkdir -p $(BUILDDIR)

clean:
	if [ -e $(T_BUILDDIR) ]; then rm -rf $(T_BUILDDIR); fi
	if [ -e $(BUILDDIR) ]; then rm -rf $(BUILDDIR); fi
	if [ -e $(CHECKDIR) ]; then rm -rf $(CHECKDIR); fi
